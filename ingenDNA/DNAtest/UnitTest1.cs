using System;
using Xunit;
using DNA;

namespace DNAtest
{
    public class UnitTest1
    {
        [Fact]
        public void getDNAvalidTest()
        {
            Assert.Equal(true, Program.GetDNA_valid("GCGTCC"));
            Assert.Equal(true, Program.GetDNA_valid("GCGTCC"));
            Assert.Equal(false, Program.GetDNA_valid("CTXTEC"));
        }
    }
}
